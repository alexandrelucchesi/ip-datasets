# Files used in the optimized version of bloomfwd

Directory naming convention: `opt_X-Y-Z`, where `2^X` is the total length of
DLA, `Y` is the max prefix length in G1 (`[24, 32] in G1`) and `Z` is the max
prefix length in G2. 

## Steps taken to generate the input prefixes files

Just run `cpe` on the `prefixes.txt` file in the "basic" directory.

## Steps taken to generate the prefixes distribution file

    $ wc -l g1.txt # 24-bit prefixes count
    $ wc -l g2.txt # 32-bit prefixes count
    Create `distrib.txt` with the corresponding quantities.
