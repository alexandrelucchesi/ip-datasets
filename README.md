# IPv4 and IPv6 test datasets

This repository contains the files used to test both
[bloomfwd](https://bitbucket.org/alexandrelucchesi/bloomfwd) and
[MIHT](https://bitbucket.org/alexandrelucchesi/miht).

## Steps taken to generate the input addresses file

Just use `ipgen` or `ipgen_v6` with the desired quantity.

## Steps taken to generate the input prefixes file

### From *-asppath* files:

    1. Download routes from (Aggregation using prepended AS Path/BGP Path):
        * http://bgp.potaroo.net/as6447/bgp-table-asppath.txt
        * http://bgp.potaroo.net/as2.0/bgp-table-asppath.txt

    2. Command to delete withdrawn routes:
        $ sed '/Withdrawn/d' bgp-table-asppath.txt > prefixes-asppath.txt

    3. Delete everything from the resulting file, except the routes in the first
       column:
        * Use `cut -f 1 -d ' ' ...` or do manually in Vim:
            - Delete contents in the right, then trim strings: `%s/\s/+$//`

    4. Remove repeated prefixes/routes:
        $ awk '!a[$0]++' prefixes-asppath.txt

    5. Add dummy next hop IP addresses in the second column:
        * $ wc -l prefixes.txt # This is the number of prefixes
        * $ ipgen <number of prefixes> > nhops.txt
        * Merge `prefixes.txt` and `nhops.txt` (Vim ;))
        * $ rm nhops.txt

### From *bgptable* files:

#### CISCO format:

    1. Command to filter routes:
        $ runhaskell helpers/CiscoParser.hs

    2. Remove repeated prefixes/routes (*should have none!*):
        $ awk '!a[$0]++' prefixes_pref2.txt > prefixes.txt

    2.1. **IPv6 only:** Rewrite routes to their canonical forms (8 groups of 4
        hex digits):
        $ runhaskell helpers/IPv6Parser.hs data/as6447-v6.txt

    3. Add dummy next hop IP addresses in the second column:
        * $ wc -l prefixes.txt # This is the number of prefixes
        * $ ipgen <number of prefixes> > nhops.txt
        * Merge `prefixes.txt` and `nhops.txt` (Vim ;))
        * $ rm nhops.txt

#### MRT format:

    1. May be easily done in Vim using block selection/deletion:
        * Useful commands:
            - Trim: `%s/\s/+$//`
            - Delete blank lines (use trim first): `%g/^$/d`

    2. Remove repeated prefixes/routes (*should have none!*):
        $ awk '!a[$0]++' prefixes-asppath.txt

    3. Add dummy next hop IP addresses in the second column:
        * $ wc -l prefixes.txt # This is the number of prefixes
        * $ ipgen <number of prefixes> > nhops.txt
        * Merge `prefixes.txt` and `nhops.txt` (Vim ;))
        * $ rm nhops.txt

## Steps taken to generate the prefixes distribution file

Just use `ipstat` or `ipstat_v6`.

